/**
 * @brief Definition of Operation for interface segregation principle
 * 
 */
#include "NewOperation.hxx"

void isp::NewOperation::op1()
{
  std::cout << "NewOperation::op1()" << std::endl;
}

void isp::NewOperation::op2()
{
  std::cout << "NewOperation::op2()" << std::endl;
}
void isp::NewOperation::op3()
{
  std::cout << "NewOperation::op3()" << std::endl;
}

