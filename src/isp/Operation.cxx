/**
 * @brief Definition of Operation for interface segregation principle
 * 
 */
#include "Operation.hxx"

void isp::Operation::op1()
{
  std::cout << "Operation::op1()" << std::endl;
}

void isp::Operation::op2()
{
  std::cout << "Operation::op2()" << std::endl;
}
void isp::Operation::op3()
{
  std::cout << "Operation::op3() + changed" << std::endl;
}

