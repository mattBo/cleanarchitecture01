/**
 * @brief Definition of User3 for interface segregation principle
 * 
 */
#include "User3.hxx"

void isp::User3::op()
{
  operation.op3();
}