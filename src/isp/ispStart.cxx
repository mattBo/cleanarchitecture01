/**
 * @brief Starts example for interface segregation principle
 * 
 */
#include "ispStart.hxx"

void isp::start()
{
  std::cout << "==================" << std::endl;
  std::cout << "Demo: interface segregation principle (ISP)" << std::endl;

  std::cout << "Bad style" << std::endl;
  User1 user1;
  user1.op();

    User2 user2;
  user2.op();

    User3 user3;
  user3.op();

  std::cout << "Good style" << std::endl;
  std::shared_ptr<NewOperation> newOperation = std::make_shared<NewOperation>();  
  NewUser1 newUser1;
  newUser1.operation = newOperation;
  newUser1.op();

  NewUser2 newUser2;
  newUser2.operation = newOperation;
  newUser2.op();

  NewUser3 newUser3;
  newUser3.operation = newOperation;
  newUser3.op();

  std::cout << "==================" << std::endl;
}
