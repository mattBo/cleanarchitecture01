/**
 * @brief Definition of NewUser1 for interface segregation principle
 * 
 */
#include "NewUser1.hxx"

void isp::NewUser1::op()
{
  operation->op1();
}