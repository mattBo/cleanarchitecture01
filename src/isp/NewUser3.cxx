/**
 * @brief Definition of NewUser3 for interface segregation principle
 * 
 */
#include "NewUser3.hxx"

void isp::NewUser3::op()
{
  operation->op3();
}