/**
 * @brief Definition of User1 for interface segregation principle
 * 
 */
#include "User1.hxx"

void isp::User1::op()
{
  operation.op1();
}