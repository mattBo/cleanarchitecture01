/**
 * @brief Definition of User2 for interface segregation principle
 * 
 */
#include "User2.hxx"

void isp::User2::op()
{
  operation.op2();
}