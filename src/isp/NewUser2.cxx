/**
 * @brief Definition of NewUser2 for interface segregation principle
 * 
 */
#include "NewUser2.hxx"

void isp::NewUser2::op()
{
  operation->op2();
}