#include <iostream>
#include "dipStart.hxx"
#include "ispStart.hxx"

int main(int argc, char const *argv[])
{
  std::cout << "ca playground" << std::endl;

  isp::start();

  dip::start();

  std::cout << "ca playground end" << std::endl;
  return 0;
}
