/**
 * @brief Definition of Application for dependency inversion principle
 * 
 */
#include "Application.hxx"

dip::Application::Application(std::shared_ptr<IServiceFactory> serviceFactory)
{
  if (serviceFactory)
  {
    service = serviceFactory->makeService();
  } else
  {
     std::cerr << "Error: ServiceFactory not defined" << std::endl;
  }
}

void dip::Application::op1()
{
  std::cout << "Application::op1()" << std::endl;
  if (service)
  {
    service->serve();
  } else
  {
    std::cerr << "Error: Service not defined" << std::endl;
  }
}
