/**
 * @brief Starts example for dependency inversion principle
 * 
 */
#include "dipStart.hxx"
#include "Application.hxx"
#include "ServiceFactoryImpl.hxx"

void dip::start()
{
  std::cout << "==================" << std::endl;
  std::cout << "Demo: dependency inversion principle (DIP)" << std::endl;

  std::shared_ptr<ServiceFactoryImpl> serviceFactory = std::make_shared<ServiceFactoryImpl>();
  Application app(serviceFactory);
  app.op1();

  std::cout << "==================" << std::endl;
}