/**
 * @brief Definition of ServiceImpl for dependency inversion principle
 * 
 */
#include "ServiceImpl.hxx"

void dip::ServiceImpl::serve()
{
  std::cout << "ServiceImpl: serve + super changed" << std::endl;
}
