/**
 * @brief Definition of ServiceFactoryImpl for dependency inversion principle
 * 
 */
#include "ServiceFactoryImpl.hxx"

std::shared_ptr<dip::IService> dip::ServiceFactoryImpl::makeService()
{
  return std::make_shared<ServiceImpl>();
}
