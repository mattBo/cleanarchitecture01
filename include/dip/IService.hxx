/**
 * @brief Declaration of Service for dependency inversion principle
 * 
 */
#pragma once

namespace dip
{
class IService
{
private:
public:
  virtual void serve() = 0;
};
} // namespace dip
