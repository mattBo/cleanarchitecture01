/**
 * @brief Declaration of Application for dependency inversion principle
 * 
 */
#pragma once

#include <iostream>
#include <IService.hxx>
#include <IServiceFactory.hxx>

namespace dip
{
class Application
{
private:
  std::shared_ptr<IService> service;
public:
  Application(std::shared_ptr<IServiceFactory> serviceFactory = nullptr);
  void op1();
  
};
} // namespace dip
