/**
 * @brief Declaration of ServiceImpl for dependency inversion principle
 * 
 */
#pragma once

#include "IService.hxx"
#include <iostream>

namespace dip
{
class ServiceImpl : public IService
{
private:
public:
  void serve() override;
};
} // namespace dip
