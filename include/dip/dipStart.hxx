/**
 * @brief Starts example for dependency inversion principle
 * 
 */
#pragma once 

#include <iostream>

namespace dip
{

void start();
  
} // namespace dip
