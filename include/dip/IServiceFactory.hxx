/**
 * @brief Declaration of ServiceFactory for dependency inversion principle
 * 
 */
#pragma once

#include "IService.hxx"
#include <memory>

namespace dip
{
class IServiceFactory
{
private:
public:
  virtual std::shared_ptr<IService> makeService() = 0;
};
} // namespace dip
