/**
 * @brief Declaration of ServiceFactoryImpl for dependency inversion principle
 * 
 */
#pragma once

#include "IServiceFactory.hxx"
#include "ServiceImpl.hxx"

namespace dip
{
class ServiceFactoryImpl : public IServiceFactory
{
private:
public:
  std::shared_ptr<IService> makeService() override;
};
} // namespace dip
