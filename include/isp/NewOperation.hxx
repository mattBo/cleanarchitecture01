/**
 * @brief Declaration of NewOperation for interface segregation principle
 * 
 */
#pragma once

#include "IOpUser1.hxx"
#include "IOpUser2.hxx"
#include "IOpUser3.hxx"
#include <iostream>

namespace isp
{
class NewOperation : 
  public IOpUser1,
  public IOpUser2,
  public IOpUser3
{
private:
public:
void op1();
void op2();
void op3();
void op4();
  
};
} // namespace isp
