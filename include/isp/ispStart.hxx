/**
 * @brief Starts example for interface segregation principle
 * 
 */
#pragma once 

#include "NewOperation.hxx"
#include "NewUser1.hxx"
#include "NewUser2.hxx"
#include "NewUser3.hxx"
#include "User1.hxx"
#include "User2.hxx"
#include "User3.hxx"

namespace isp
{

void start();
  
} // namespace isp
