/**
 * @brief Declaration of IUser1Op for interface segregation principle
 * 
 */
#pragma once

#include <iostream>

namespace isp
{
class IOpUser1
{
private:
public:
virtual void op1() = 0;
  
};
} // namespace isp
