/**
 * @brief Declaration of User2 for interface segregation principle
 * 
 */
#pragma once

#include "IOpUser2.hxx"

namespace isp
{
class NewUser2
{
private:
public:
  std::shared_ptr<isp::IOpUser2> operation;
  void op();
};
} // namespace isp
