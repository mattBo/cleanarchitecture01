/**
 * @brief Declaration of User2 for interface segregation principle
 * 
 */
#pragma once

#include "Operation.hxx"

namespace isp
{
class User2
{
private:
  isp::Operation operation;
public:
  void op();
};
} // namespace isp
