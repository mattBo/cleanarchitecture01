/**
 * @brief Declaration of IOpUser2 for interface segregation principle
 * 
 */
#pragma once

#include <iostream>

namespace isp
{
class IOpUser2
{
private:
public:
virtual void op2() = 0;
  
};
} // namespace isp
