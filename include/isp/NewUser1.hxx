/**
 * @brief Declaration of User1 for interface segregation principle
 * 
 */
#pragma once

#include "IOpUser1.hxx"

namespace isp
{
class NewUser1
{
private:
public:
  std::shared_ptr<isp::IOpUser1> operation;
  void op();
};
} // namespace isp
