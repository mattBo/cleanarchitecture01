/**
 * @brief Declaration of User3 for interface segregation principle
 * 
 */
#pragma once

#include "Operation.hxx"

namespace isp
{
class User3
{
private:
  isp::Operation operation;
public:
  void op();
};
} // namespace isp
