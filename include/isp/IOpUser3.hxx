/**
 * @brief Declaration of IOpUser3 for interface segregation principle
 * 
 */
#pragma once

#include <iostream>

namespace isp
{
class IOpUser3
{
private:
public:
virtual void op3() = 0;
  
};
} // namespace isp
