/**
 * @brief Declaration of User3 for interface segregation principle
 * 
 */
#pragma once

#include "IOpUser3.hxx"

namespace isp
{
class NewUser3
{
private:
public:
  std::shared_ptr<isp::IOpUser3> operation;
  void op();
};
} // namespace isp
