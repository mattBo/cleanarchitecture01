/**
 * @brief Declaration of Operation for interface segregation principle
 * 
 */
#pragma once

#include <iostream>

namespace isp
{
class Operation
{
private:
public:
void op1();
void op2();
void op3();
void op4();
  
};
} // namespace isp
