/**
 * @brief Declaration of User1 for interface segregation principle
 * 
 */
#pragma once

#include "Operation.hxx"

namespace isp
{
class User1
{
private:
  isp::Operation operation;
public:
  void op();
};
} // namespace isp
